#!/bin/bash

# get bundle plugin
rm -rf ~/.vim/bundle
git clone https://gitee.com/mirrors/vundle-vim.git ~/.vim/bundle/Vundle.vim

# update vimrc
cp vimrc ~/.vimrc
