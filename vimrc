
" --------------- Vundle start ---------------
set nocompatible              " be iMproved, required
filetype off                  " required

" set the runtime path to include Vundle and initialize
set rtp+=~/.vim/bundle/Vundle.vim
call vundle#begin()
" alternatively, pass a path where Vundle should install plugins
"call vundle#begin('~/some/path/here')

" let Vundle manage Vundle, required
Plugin 'VundleVim/Vundle.vim'

" fzf
" Plugin 'junegunn/fzf'
Plugin 'https://gitee.com/mirrors/fzf'

" nerdtree
" Plugin 'preservim/nerdtree'
Plugin 'https://gitee.com/mirrors/nerdtree'
call vundle#end()            " required
filetype plugin indent on    " required
" --------------- Vundle end ---------------

" fav settings
set fileencodings=utf-8,ucs-bom,gb18030,gbk,gb2312,cp936
set termencoding=utf-8
set encoding=utf-8
set tabstop=4
set expandtab

" NerdTree
:abbreviate nd NERDTreeToggle

" fzf
:abbreviate ff FZF


